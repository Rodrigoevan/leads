package com.br.lead.collector.controllers;

import com.br.lead.collector.enums.TipoDeLead;
import com.br.lead.collector.models.Lead;
import com.br.lead.collector.models.Produto;
import com.br.lead.collector.repositories.LeadRepository;
import com.br.lead.collector.services.LeadService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@WebMvcTest(LeadController.class)
public class LeadControllerTests {

    @MockBean
    LeadService leadService;

    @MockBean
    LeadRepository leadRepository;

    @Autowired
    private MockMvc mockMvc;

    //converte Objeto para json
    ObjectMapper mapper = new ObjectMapper();

    private Lead leadConst = new Lead("Rodrigo Evangelista",
            "rodrigoevan@hotmail.com", TipoDeLead.QUENTE);
    private Produto produtoConst = new Produto(1,"café",
            "Melita", 10.0);

    private Lead lead = new Lead();
    private Produto produto = new Produto();

    @BeforeEach
    public void iniciar(){
        produto.setNome("Café");
        produto.setId(1);
        produto.setDescricao("marca pilao");
        produto.setPreco(10.00);

        lead.setId(1);
        lead.setNome("Rodrigo Evangelista");
        lead.setEmail("rodrigoevan@hotmail.com");
        lead.setTipoDeLead(TipoDeLead.QUENTE);
        lead.setProdutos(Arrays.asList(produto));

    }
    @Test
    public void testarInserirLead() throws Exception {

        Iterable<Produto> produtosIterable = Arrays.asList(produto);

        Mockito.when(leadService.salvarLead(Mockito.any(Lead.class))).thenReturn(lead);
        Mockito.when(leadService.buscarTodosProdutos(Mockito.anyList())).thenReturn(produtosIterable);

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.post("/lead/salvar")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers
                        .status().isCreated())
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.produtos[0].id", CoreMatchers.equalTo(1)));

    }

    @Test
    public void testarBuscarLead() throws Exception {
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.ofNullable(lead));

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/lead/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.produtos[0].id", CoreMatchers.equalTo(1)));

    }

    @Test
    public void testarBuscarLeadnNaoEncontrado() throws Exception {
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.empty());

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/lead/1"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

    }

    @Test
    public void testarBuscarTodosLeads() throws Exception {
        Iterable<Lead> leadsIterable = Arrays.asList(lead);
        Mockito.when(leadService.buscarTodosLeads()).thenReturn(leadsIterable);

        String json = mapper.writeValueAsString(leadsIterable);

        mockMvc.perform(MockMvcRequestBuilders.get("/lead/todosLeads"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));

    }

    @Test
    public void testarAtualizarLead() throws Exception{

        Mockito.when(leadService.atualizarLead(Mockito.any(Lead.class))).thenReturn(lead);

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.put("/lead/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers
                        .status().isOk())
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.produtos[0].id", CoreMatchers.equalTo(1)));

    }
    @Test
    public void testarDeletarLead() throws Exception{
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.ofNullable(lead));

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.delete("/lead/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers
                        .status().isOk());

    }
    @Test
    public void testarDeletarLeadInexistente() throws Exception{
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.empty());

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.delete("/lead/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers
                        .status().isNoContent());

    }

}

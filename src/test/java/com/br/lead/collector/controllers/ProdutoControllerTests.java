package com.br.lead.collector.controllers;

import com.br.lead.collector.models.Lead;
import com.br.lead.collector.models.Produto;
import com.br.lead.collector.repositories.ProdutoRepository;
import com.br.lead.collector.services.ProdutoService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(ProdutoController.class)
public class ProdutoControllerTests {

    @MockBean
    ProdutoService produtoService;

    @MockBean
    ProdutoRepository produtoRepository;

    @Autowired
    private MockMvc mockMvc;

    //converte Objeto para json
    ObjectMapper mapper = new ObjectMapper();

    Produto produto = new Produto();

    @BeforeEach
    public void iniciar(){

        produto.setNome("Café");
        produto.setId(1);
        produto.setDescricao("marca pilao");
        produto.setPreco(10.00);

    }

    @Test
    public void testarBuscarTodosProdutos() throws Exception {
        Iterable<Produto> produtosIterable = Arrays.asList(produto);
        Mockito.when(produtoService.buscarTodosProdutos()).thenReturn(produtosIterable);

        String json = mapper.writeValueAsString(produtosIterable);

        mockMvc.perform(MockMvcRequestBuilders.get("/produto/todosProdutos"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void testarBuscarProduto() throws Exception{
        Mockito.when(produtoService.buscarPorid(Mockito.anyInt()))
                .thenReturn(Optional.ofNullable(produto));

        String json = mapper.writeValueAsString(produto);

         mockMvc.perform(MockMvcRequestBuilders.get("/produto/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.id", CoreMatchers.equalTo(1)));

    }
    @Test
    public void testarBuscarProdutoNaoEncontrado() throws Exception{
        Mockito.when(produtoService.buscarPorid(Mockito.anyInt()))
                .thenReturn(Optional.empty());

        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.get("/produto/1"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

    }

    @Test
    public void testarSalvarProduto() throws Exception {
        Iterable<Produto> produtosIterable = Arrays.asList(produto);

        Mockito.when(produtoService.salvarProduto(Mockito.any(Produto.class))).thenReturn(produto);

        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.post("/produto/salvar")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers
                        .status().isCreated())
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.id", CoreMatchers.equalTo(1)));


    }
    @Test
    public void testarAtualizarProduto() throws Exception {
        Mockito.when(produtoService.atualizarProduto(Mockito.any(Produto.class))).thenReturn(produto);

        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.put("/produto/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers
                        .status().isOk())
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.id", CoreMatchers.equalTo(1)));

    }
    @Test
    public void testarDeletarProduto () throws Exception {
        Mockito.when(produtoService.buscarPorid(Mockito.anyInt())).thenReturn(Optional.ofNullable(produto));

        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.delete("/produto/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers
                        .status().isOk());
    }
    @Test
    public void testarDeletarProdutoNaoEncontrado () throws Exception {
        Mockito.when(produtoService.buscarPorid(Mockito.anyInt())).thenReturn(Optional.empty());

        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.delete("/produto/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers
                        .status().isNoContent());

    }


}


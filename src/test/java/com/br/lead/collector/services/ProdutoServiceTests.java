package com.br.lead.collector.services;

import com.br.lead.collector.models.Lead;
import com.br.lead.collector.models.Produto;
import com.br.lead.collector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class ProdutoServiceTests {

    @MockBean
    private ProdutoRepository produtoRepository;

    @Autowired
    ProdutoService produtoService;

    private Produto produto1 = new Produto();
    private Produto produto2 = new Produto();
    private Produto produtoAlterado = new Produto();

    @BeforeEach
    public void inicializarProduto(){
        produto1.setNome("Café");
        produto1.setId(1);
        produto1.setDescricao("marca pilao");
        produto1.setPreco(10.00);

        produto2.setNome("Leite");
        produto2.setId(2);
        produto2.setDescricao("semidesnatado paulista");
        produto2.setPreco(3.00);

        produtoAlterado.setNome("Café");
        produtoAlterado.setId(1);
        produtoAlterado.setDescricao("marca pilao");
        produtoAlterado.setPreco(10.00);


    }
    @Test
    public void testarBuscarPorid(){
        Mockito.when(produtoRepository.findById(produto1.getId())).thenReturn(Optional.of(produto1));
        Optional<Produto> produtoObjeto = produtoService.buscarPorid(produto1.getId());
        Mockito.verify(produtoRepository).findById(produto1.getId());
        Assertions.assertEquals(produtoObjeto, Optional.of(produto1));
    }
    @Test
    public void testarSalvarProduto(){
        Mockito.when(produtoRepository.save(produto1)).thenReturn(produto1);
        Produto prodtoObjeto = produtoService.salvarProduto(produto1);
        Mockito.verify(produtoRepository).save(produto1);
        Assertions.assertEquals(prodtoObjeto, produto1);
    }

    @Test
    public void testarBuscarTodosProdutos(){
        Iterable<Produto> produtosIterable = Arrays.asList(produto1,produto2);
        Mockito.when(produtoRepository.findAll()).thenReturn(produtosIterable);
        Iterable<Produto> produtosObjeto = produtoService.buscarTodosProdutos();
        Mockito.verify(produtoRepository).findAll();
        Assertions.assertEquals(produtosObjeto, produtosIterable);
    }
    @Test
    public void testarAtualizarProdutoNome(){
        produtoAlterado.setPreco(produto1.getPreco());
        produtoAlterado.setId(produto1.getId());
        produtoAlterado.setDescricao(produto1.getDescricao());
        produtoAlterado.setNome(produto1.getNome());
        produtoAlterado.setNome("Chá");
        Mockito.when(produtoRepository.findById(produto1.getId()))
                .thenReturn(Optional.of(produto1));
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class)))
                .thenReturn(produtoAlterado);
        Produto produtoObjeto = produtoService.atualizarProduto(produtoAlterado);
        Mockito.verify(produtoRepository).save(produtoAlterado);
        Assertions.assertEquals(produtoObjeto, produtoAlterado);

    }
    @Test
    public void testarAtualizarProdutoNomeNull(){
        produtoAlterado.setPreco(produto1.getPreco());
        produtoAlterado.setId(produto1.getId());
        produtoAlterado.setDescricao(produto1.getDescricao());
        produtoAlterado.setNome(produto1.getNome());
        produtoAlterado.setNome(null);
        Mockito.when(produtoRepository.findById(produto1.getId()))
                .thenReturn(Optional.of(produto1));
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class)))
                .thenReturn(produto1);
        Produto produtoObjeto = produtoService.atualizarProduto(produtoAlterado);
        Assertions.assertEquals(produtoObjeto.getNome(), produtoAlterado.getNome());
        Assertions.assertEquals(produtoObjeto.getId(), produtoAlterado.getId());
        Assertions.assertEquals(produtoObjeto.getDescricao(), produtoAlterado.getDescricao());
        Assertions.assertEquals(produtoObjeto.getPreco(), produtoAlterado.getPreco());

    }
    @Test
    public void testarAtualizarProdutoDescricaoNull(){
        produtoAlterado.setPreco(produto1.getPreco());
        produtoAlterado.setId(produto1.getId());
        produtoAlterado.setDescricao(produto1.getDescricao());
        produtoAlterado.setNome(produto1.getNome());
        produtoAlterado.setDescricao(null);
        Mockito.when(produtoRepository.findById(produto1.getId()))
                .thenReturn(Optional.of(produto1));
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class)))
                .thenReturn(produto1);
        Produto produtoObjeto = produtoService.atualizarProduto(produtoAlterado);
        Assertions.assertEquals(produtoObjeto.getDescricao(), produtoAlterado.getDescricao());
        Assertions.assertEquals(produtoObjeto.getNome(), produtoAlterado.getNome());
        Assertions.assertEquals(produtoObjeto.getId(), produtoAlterado.getId());
        Assertions.assertEquals(produtoObjeto.getPreco(), produtoAlterado.getPreco());

    }
    @Test
    public void testarAtualizarProdutoPrecoNull(){
        produtoAlterado.setPreco(produto1.getPreco());
        produtoAlterado.setId(produto1.getId());
        produtoAlterado.setDescricao(produto1.getDescricao());
        produtoAlterado.setNome(produto1.getNome());
        produtoAlterado.setPreco(null);
        Mockito.when(produtoRepository.findById(produto1.getId()))
                .thenReturn(Optional.of(produto1));
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class)))
                .thenReturn(produto1);
        Produto produtoObjeto = produtoService.atualizarProduto(produtoAlterado);
        Assertions.assertEquals(produtoObjeto.getPreco(), produtoAlterado.getPreco());
        Assertions.assertEquals(produtoObjeto.getDescricao(), produtoAlterado.getDescricao());
        Assertions.assertEquals(produtoObjeto.getNome(), produtoAlterado.getNome());
        Assertions.assertEquals(produtoObjeto.getId(), produtoAlterado.getId());


    }
    @Test
    public void testarDeletarProduto(){
        produtoService.deletarProduto(produto1);
        Mockito.verify(produtoRepository).delete(Mockito.any(Produto.class));
    }

}

package com.br.lead.collector.services;

import com.br.lead.collector.enums.TipoDeLead;
import com.br.lead.collector.models.Lead;
import com.br.lead.collector.models.Produto;
import com.br.lead.collector.repositories.LeadRepository;
import com.br.lead.collector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.*;

@SpringBootTest
public class LeadServiceTests {

    @MockBean
    LeadRepository leadRepository;

    @MockBean
    ProdutoRepository produtoRepository;

    @Autowired
    LeadService leadService;

    private Lead lead = new Lead();;
    private Lead leadAlterado = new Lead();
    private Produto produto1 = new Produto();
    private Produto produto2 = new Produto();
    private List<Integer> produtosID = new ArrayList<>();

    @BeforeEach
    public void inicializarLead(){
        produto1.setNome("Café");
        produto1.setId(1);
        produto1.setDescricao("marca pilao");
        produto1.setPreco(10.00);

        produto2.setNome("Leite");
        produto2.setId(2);
        produto2.setDescricao("semidesnatado paulista");
        produto2.setPreco(3.00);

        produtosID.add(produto1.getId());
        produtosID.add(produto2.getId());

        lead.setId(1);
        lead.setTipoDeLead(TipoDeLead.QUENTE);
        lead.setEmail("rodrigoevan@hotmail.com");
        lead.setNome("Rodrigo Evangelista");
        lead.setProdutos(Arrays.asList(new Produto()));

        leadAlterado.setId(1);
        leadAlterado.setTipoDeLead(TipoDeLead.QUENTE);
        leadAlterado.setEmail("rodrigoevan@hotmail.com");
        leadAlterado.setNome(null);
        leadAlterado.setProdutos(Arrays.asList(new Produto()));

    }

    @Test
    public void testarSalvarLead(){

        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);
        Lead leadObjeto = leadService.salvarLead(lead);

        Assertions.assertEquals(lead, leadObjeto);
        Assertions.assertEquals(lead.getEmail(),leadObjeto.getEmail());
    }
    @Test
    public void testarDeletarLead(){
        leadService.deletarLead(lead);
        Mockito.verify(leadRepository).delete(Mockito.any(Lead.class));
    }
    @Test
    public void testarBuscarTodosLeads(){
        Iterable<Lead> leadsIterable = Arrays.asList(lead);
        Mockito.when(leadRepository.findAll())
                .thenReturn(leadsIterable);
        Iterable<Lead> leads = leadService.buscarTodosLeads();
        Mockito.verify(leadRepository).findAll();
        Assertions.assertEquals(leads,leadsIterable);
    }

    @Test
    public void testarBuscarPorId(){
        leadService.buscarPorId(lead.getId());
        Mockito.verify(leadRepository).findById(1);
    }
    @Test
    public void testarBuscarTodosProdutos(){
        leadService.buscarTodosProdutos(produtosID);
        Mockito.verify(produtoRepository).findAllById(produtosID);
    }
    @Test
    public void testarAtualizarLeadNome(){

        Mockito.when(leadRepository.save(Mockito.any(Lead.class)))
                .thenReturn(leadAlterado);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(lead));
        leadAlterado.setNome("Manuel");
        Lead leadObjeto = leadService.atualizarLead(leadAlterado);
        Assertions.assertEquals(leadAlterado.getNome(), leadObjeto.getNome());
    }

    @Test
    public void testarAtualizarLeadNomeNull(){
        leadAlterado.setNome(null);
        Mockito.when(leadRepository.save(Mockito.any(Lead.class)))
                .thenReturn(lead);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(lead));
        Lead leadObjeto = leadService.atualizarLead(leadAlterado);
        Assertions.assertEquals(lead.getNome(), leadObjeto.getNome());
    }
    @Test
    public void testarAtualizarLeadEmailNull(){
        leadAlterado.setEmail(null);
        Mockito.when(leadRepository.save(Mockito.any(Lead.class)))
                .thenReturn(lead);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(lead));
        Lead leadObjeto = leadService.atualizarLead(leadAlterado);
        Assertions.assertEquals(lead.getEmail(), leadObjeto.getEmail());
    }
    @Test
    public void testarAtualizarLeadTipoDeLeadNull(){
        leadAlterado.setTipoDeLead(null);
        Mockito.when(leadRepository.save(Mockito.any(Lead.class)))
                .thenReturn(lead);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(lead));
        Lead leadObjeto = leadService.atualizarLead(leadAlterado);
        Assertions.assertEquals(lead.getTipoDeLead(), leadObjeto.getTipoDeLead());
    }

}

package com.br.lead.collector.controllers;

import com.br.lead.collector.models.Produto;
import com.br.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Optional;

@RestController
@RequestMapping("/produto")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @GetMapping("/todosProdutos")
    public Iterable<Produto> buscarTodosProdutos(){
        return produtoService.buscarTodosProdutos();
    }

    @GetMapping("/{id}")
    public Produto buscarProduto(@PathVariable Integer id){
        Optional<Produto> produtoOptional = produtoService.buscarPorid(id);
        if(produtoOptional.isPresent()){
            return  produtoOptional.get();
        }else{
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("salvar")
    public ResponseEntity<Produto> salvarProduto(@RequestBody Produto produto){
        produtoService.salvarProduto(produto);
        return ResponseEntity.status(201).body(produto);
    }

    @PutMapping("/{id}")
    public Produto atualizarProduto(@PathVariable Integer id, @RequestBody Produto produto){
        produto.setId(id);
        Produto produtoObjeto = produtoService.atualizarProduto(produto);
        return produtoObjeto;
    }
    @DeleteMapping("/{id}")
    public Produto deletarProduto(@PathVariable Integer id){
        Optional<Produto> produtoOptional = produtoService.buscarPorid(id);
        if (produtoOptional.isPresent()){
            produtoService.deletarProduto(produtoOptional.get());
        }else{
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
        return produtoOptional.get();
    }
}
